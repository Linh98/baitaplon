import React, { Component } from 'react';
import './App.css';

 
class App extends Component {
  render() {
    return (
      <div id="wrapper">
     <div id="header">
	   <img src="template/banner.jpg" alt="Danh Sách Thiết Bị"/>
	 </div>
	 <div id="menu">
	   <ul>
	     <li><a href="a.html">Trang Chủ</a></li>
		 <li><a href="a.html">Giới Thiệu</a></li>
		 <li><a href="a.html">Thiết Bị</a></li>
		 <li><a href="a.html">Trạng Thái</a></li>
		</ul>
     </div>
     
     <div id="pane">
        <div className="panitem" id="left" />
        <div className="panitem" id="content">
		  <fieldset>
		     <legend> Form Cập Nhật Trạng Thái Thiết Bị</legend>
			 <form action="a.php" method="POST">
			    <table>
				 <tbody><tr>
				    <td width="100px">Username</td>
					<td><input type="text" name="txtUser" /></td>
					</tr>
				 
                   <tr>
                    <td>Tình trạng</td>
					<input type="radio" name="rdoTinhtrang" defaultValue={0}  /> Tốt
					<input type="radio" name="rdoTinhtramg" defaultValue={0}  /> Hỏng
				   </tr>
				   <tr>
				    <td> Chọn Thiết Bị</td>
					<td>
					   <select name="sltThietbi">
					     <option value="mh">Màn Hình</option>
						 <option value="vmt">Vỏ Máy Tính</option>
						 <option value="oc">Ổ Cứng</option>
						 <option value="od">Ổ Đĩa</option>
						 <option value="c">Chuột</option>
						 <option value="bp">Bàn Phím</option>
					   </select>
                    </td>
                   </tr>					
                   <tr>	
                    <td>Note</td>
                    <td>
                       <textarea name="txtNote" cols={20} rows={3} defaultValue={""} />
                    </td>
				   </tr>
                   <tr>
                    <td colSpan={2} align="center">
                       <input type="Submit" name="btnSubmit" defaultValue="Gửi Form" />
                   
                    </td>
				   </tr>	
                  </tbody>
				 </table>				 
			 </form>
          </fieldset>
    </div>
	    <div className="panitem" id="right" />
	</div>
	<div className="clear" />
	<div id="lower">
	    <div id="left" className="font">ReactJS</div>
        <div id="right" className="font">NodeJS</div>
    </div>
    <div className="clear" />
    <div id="footer" className="font"></div>
    
    </div>	
          		
 

    );
  }
}
 
export default App;